﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
	void Update ()
	{
		if(Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, -10f));
			Physics.Raycast (ray, out hit, 1000);
//			Debug.DrawRay (ray.origin, ray.direction * 100f, Color.yellow, 2f);
			if(hit.transform)
			{
				SphereUnit currentTarget = hit.transform.gameObject.GetComponent<SphereUnit> ();
				if(currentTarget != null)
				{
					currentTarget.OnPlayerClick ();
				}
			}
		}
	}
}
