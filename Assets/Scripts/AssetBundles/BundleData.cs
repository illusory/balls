﻿using UnityEngine;
using System.Collections;

public delegate void OnFinishLoad();

public class BundleData
{
	public event OnFinishLoad finishLoadEvent;
	
	public bool isLoaded = false;
	public AssetBundle loadedBundle;
	public float timeToUnload;
	public int freezeCount = 0;
	
	public void RaiseEvent()
	{
		if(finishLoadEvent != null)
		{
			finishLoadEvent();
			finishLoadEvent = null;
		}
	}
}