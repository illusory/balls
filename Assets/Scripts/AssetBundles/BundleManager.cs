﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Runtime.InteropServices;

public delegate void BundleLoadingProgress(float progress);
public delegate void DownloadProcessResult(bool result);

public class BundleManager : MonoBehaviour
{
	public enum BuildPlatforms { Android, iOS, WEB, WebGL, Standalone, WSA, WP8 };
	public static int clientAssetBundlesVersion = 1;
	public static Dictionary<string, BundleData> loadedBundles = new Dictionary<string, BundleData>();
	public static List<BundleRow> bundleRowsInManifest = new List<BundleRow>();
	public static List<BundleRow> bundlesForDownload = new List<BundleRow>();

	public static DownloadProcessResult onFinishDownloadProcess;

	public static WWW download;
	public static long size_to_download = 0;
	public static long size_downloaded = 0;
	public static float startProgress = 0f;
	public static float endProgress = 0f;
	public static bool isBundleLoadingNow = false;
	public static bool isAppUpdated = false;

	public static bool isBundleSystemInitialized = false;


	static BundleManager instance;
	public static BundleManager Instance
	{
		get
		{
			if(instance == null)
			{
				BundleManager findedObject = GameObject.FindObjectOfType<BundleManager>();
				if(findedObject == null)
				{
					GameObject bandleManagerObject = new GameObject("BundleManager");
					instance = bandleManagerObject.AddComponent<BundleManager>();
					GameObject.DontDestroyOnLoad(bandleManagerObject);
				}
				else
				{
					instance = findedObject;
				}
			}

			return instance;
		}
	}
	
	public static bool ReadAssetBundleManifest()
	{
		Stream reader;
		bundleRowsInManifest.Clear();
		try
		{
			reader = new MemoryStream( (Resources.Load("PluginParams/AssetBundleManifest" + GetPlatformName(), typeof(TextAsset)) as TextAsset).bytes );			
			StreamReader textReader = new StreamReader(reader);
			while (!textReader.EndOfStream)
			{
				BundleRow row = new BundleRow(textReader.ReadLine());
				bundleRowsInManifest.Add(row);
			}
			reader.Dispose();

			return true;
		}
		catch (System.Exception ex)
		{
			Debug.Log("Error when read AssetBundleManifest: "+ ex.Message);
			Debug.Log(ex.Message);
			return false;
		}
	}

	public static bool CheckAssetBundles()
	{
		bool isBundlesReady= true;
		for(int i = 0; i < bundleRowsInManifest.Count; i++)
		{
			string bundleName = GetDevicePath() + "/" + clientAssetBundlesVersion.ToString() + "/" + bundleRowsInManifest[i].bundleName;
			if(!IsFileExist(bundleName))
			{
				isBundlesReady = false;
				size_to_download += bundleRowsInManifest[i].bundleSyzeInBytes;
				bundlesForDownload.Add(bundleRowsInManifest[i]);
			}
			else if(GetFileLength(bundleName) != bundleRowsInManifest[i].bundleSyzeInBytes)
			{
				isAppUpdated = true;
				isBundlesReady = false;
				size_to_download += bundleRowsInManifest[i].bundleSyzeInBytes;
				bundlesForDownload.Add(bundleRowsInManifest[i]);
      		}
			else {
				isAppUpdated = true;

			}
		}
		return isBundlesReady;
	}

	public static IEnumerator DownloadAndCache()
	{
		string destinationDirectory = GetDevicePath() +"/"+ clientAssetBundlesVersion.ToString();
		if (!Directory.Exists(destinationDirectory))
		{
			CreateDirectory(destinationDirectory);
		}
		size_downloaded = 0;

		for (int i = 0; i < bundlesForDownload.Count; i++)
		{
			string destinationFileName = destinationDirectory +"/"+ bundlesForDownload[i].bundleName;
		
			//------------------Download new bundles-----------!!

			isBundleLoadingNow = true;

			startProgress = (float)size_downloaded / (float)size_to_download;
			endProgress = (size_downloaded + bundlesForDownload[i].bundleSyzeInBytes) / (float)size_to_download;

			download = new WWW(GetServerLink() + bundlesForDownload[i].bundleName);
			while (!download.isDone && string.IsNullOrEmpty(download.error)) yield return null;

			isBundleLoadingNow = false;

			byte[] bytes = download.bytes;
			try
			{
				FileStream file = new FileStream(destinationFileName, FileMode.Create);
				BinaryWriter binary = new BinaryWriter(file);
				binary.Write(bytes);
				binary.Close();
				file.Dispose();
			}
			catch (Exception ex)
			{
				Debug.Log (ex.ToString());
			}
			bytes = null;
			download.Dispose();
			download = null;

			size_downloaded += bundlesForDownload[i].bundleSyzeInBytes;
		}

		isBundleSystemInitialized = true;
		if(onFinishDownloadProcess != null)
		{
			onFinishDownloadProcess(true);
		}
	}


	public static bool IsDirectoryExist(string directoryPath)
	{
		return Directory.Exists(directoryPath);
	}

	public static string GetDevicePath()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			return Application.persistentDataPath.Replace("/Documents", "") + "/Library/Application Support/Caches";
		}
		else
		{
			return Application.persistentDataPath;
		}
	}

	public static bool IsFileExist(string filePath)
	{
		return File.Exists(filePath);
	}

	public static string GetServerLink()
	{
		string serverAdres = "http://dotsout.com/TestBundles/";

		serverAdres += GetPlatformName() + "/";
		serverAdres += clientAssetBundlesVersion.ToString() + "/";
		return serverAdres;
	}

	public static AssetBundle GetAssetBundle(string bundleName)
	{
		bundleName = bundleName.ToLower();
		if (loadedBundles.ContainsKey(bundleName) && loadedBundles[bundleName].isLoaded)
		{
			loadedBundles[bundleName].timeToUnload = 10f;
			return loadedBundles[bundleName].loadedBundle;
		}
		else
		{
			AssetBundle tempBundle = null;
			string bundle_path = GetDevicePath() + "/" + clientAssetBundlesVersion.ToString() + "/" + bundleName;
			byte[] bundleBytes = File.ReadAllBytes(bundle_path);
			tempBundle = AssetBundle.LoadFromMemory(bundleBytes);
			bundleBytes = null;

			if(!loadedBundles.ContainsKey(bundleName))
			{
				BundleData newData = new BundleData();
				newData.loadedBundle = tempBundle;
				newData.isLoaded = true;
				newData.timeToUnload = 10f;

				loadedBundles.Add(bundleName, newData);

			}
			else if(loadedBundles[bundleName] == null)
			{
				loadedBundles[bundleName] = new BundleData();
				loadedBundles[bundleName].loadedBundle = tempBundle;
				loadedBundles[bundleName].isLoaded = true;
				loadedBundles[bundleName].timeToUnload = 10f;
			}

			return tempBundle;
		}
	}


	public static List<string> loadingQueue = new List<string>();
	public static void GetAssetBundleAsync(string bundleName, OnFinishLoad onLoadDelegate)
	{
		bundleName = bundleName.ToLower();
		if (loadedBundles.ContainsKey(bundleName))
		{
			if(loadedBundles[bundleName].isLoaded == true)
			{
				loadedBundles[bundleName].timeToUnload = 10f;
				onLoadDelegate();
				return;
			}
			else
			{
				loadedBundles[bundleName].finishLoadEvent += onLoadDelegate;
				return;
			}
		}
		else
		{
			BundleData newData = new BundleData();
			newData.isLoaded = false;
			newData.finishLoadEvent += onLoadDelegate;
			loadedBundles.Add(bundleName, newData);

			loadingQueue.Add(bundleName);
			if(loadingQueue.Count == 1)
			{
				Instance.StartCoroutine(AsyncLoadBundle(bundleName));
			}
		}
	}

	public static bool isNeedLockUnloadProcess = false;
	public static string currentLoadingBundleName = "";
	public static IEnumerator AsyncLoadBundle(string bundleName)
	{
		bundleName = bundleName.ToLower();

		if(bundleName != currentLoadingBundleName)
		{
			isNeedLockUnloadProcess = true;
			isBundleLoadingNow = true;
			currentLoadingBundleName = bundleName;

			AssetBundle assetBundle;

			string path = GetDevicePath() + "/" + clientAssetBundlesVersion.ToString() + "/" + bundleName;
			if(Application.platform == RuntimePlatform.WindowsEditor)
			{
				path =  Application.persistentDataPath.Replace("/", "\\") + "\\" + clientAssetBundlesVersion.ToString() + "\\" + bundleName;
			}
			if(!loadedBundles[bundleName].isLoaded)
			{
				AssetBundleCreateRequest assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(path);
				yield return assetBundleCreateRequest;
				assetBundle = assetBundleCreateRequest.assetBundle;
				assetBundleCreateRequest = null;
			}
			else
			{
				assetBundle = loadedBundles[bundleName].loadedBundle;
			}
			
			isBundleLoadingNow = false;

			loadedBundles[bundleName].isLoaded = true;
			loadedBundles[bundleName].loadedBundle = assetBundle;
			loadedBundles[bundleName].timeToUnload = 10f;

			loadingQueue.Remove(bundleName);
			currentLoadingBundleName = "";

			loadedBundles[bundleName].RaiseEvent();

			if(loadingQueue.Count > 0)
			{
				Instance.StartCoroutine(AsyncLoadBundle(loadingQueue[0]));
			}
			else
			{
				isNeedLockUnloadProcess = false;
			}
		}
	}

	public static int GetBundleVersion(string bundleName)
	{
		for (int i = 0; i < bundleRowsInManifest.Count; i++)
		{
			if(bundleRowsInManifest[i].bundleName == bundleName)
			{
				return bundleRowsInManifest[i].bundleVersion;
			}
		}
		return 0;
	}
	
	public static void ReleaseEvent(string bundleName, OnFinishLoad onLoadDelegate)
	{
		bundleName = bundleName.ToLower();
		if(loadedBundles.ContainsKey(bundleName)){
			loadedBundles[bundleName].finishLoadEvent -= onLoadDelegate;
		}
	}

	public static bool IsBundleAlreadyLoaded(string bundleName)
	{
		bundleName = bundleName.ToLower();
		if(loadedBundles.ContainsKey(bundleName) && loadedBundles[bundleName].isLoaded){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static void FreezeAssetBundle(string bundleName)
	{
		if (loadedBundles.ContainsKey(bundleName)){
			loadedBundles[bundleName].freezeCount++;
			loadedBundles[bundleName].timeToUnload = 10f;
		}
	}
	
	public static void UnFreezeAssetBundle(string bundleName)
	{
		if (loadedBundles.ContainsKey(bundleName)){
			if(loadedBundles[bundleName].freezeCount > 0){
				loadedBundles[bundleName].freezeCount--;
			}
		}
	}

	public static void ForceUnloadAssetBundle(string bundleName)
	{
    	if (loadedBundles.ContainsKey(bundleName) && loadedBundles[bundleName].loadedBundle != null && loadedBundles[bundleName].freezeCount == 0)
		{
			loadedBundles[bundleName].loadedBundle.Unload(false);
			loadedBundles.Remove(bundleName);
			unloadedCount++;
		}
	}

	public static int unloadedCount = 0;
    public static string[] idForDelete = new string[20];
    public static float updateInterval = 1f;

	public static void UpdateBundlesLiveTime()
	{
		if(isNeedLockUnloadProcess)
		{
			return;
		}

        updateInterval -= Time.deltaTime;
        if(updateInterval > 0f)
        {
            return;
        }
        else
        {
            updateInterval = 1f;
        }

        int deleteIndex = 0;

        foreach(var bundleItem in loadedBundles)
        {
            if(bundleItem.Value.isLoaded && bundleItem.Value.freezeCount == 0)
            {
                bundleItem.Value.timeToUnload -= 1f;
                if(bundleItem.Value.timeToUnload <= 0)
                {
                    if(bundleItem.Value.loadedBundle != null)
                    {
                        bundleItem.Value.loadedBundle.Unload(false);
                    }
                    idForDelete[deleteIndex] = bundleItem.Key;
                    deleteIndex++;
                }
            }
        }

        for(int i = 0; i < deleteIndex; i++)
        {
            loadedBundles.Remove(idForDelete[i]);
            unloadedCount++;
        }

		if(unloadedCount > 10)
		{
			Resources.UnloadUnusedAssets();
			unloadedCount = 0;
		}
	}

	void Update ()
	{
		UpdateBundlesLiveTime ();
	}

	public static string GetPlatformName()
	{
#if UNITY_ANDROID
		return BuildPlatforms.Android.ToString();
#elif UNITY_IPHONE
		return BuildPlatforms.iOS.ToString();
#elif UNITY_WEBPLAYER
		return BuildPlatforms.WEB.ToString();
#elif UNITY_WP8
		return BuildPlatforms.WP8.ToString();
#elif UNITY_WSA
		return BuildPlatforms.WSA.ToString();
#elif UNITY_WEBGL
		return BuildPlatforms.WebGL.ToString();
#else
		return BuildPlatforms.Standalone.ToString();
#endif
	}

	public static long GetFileLength(string filePath)
	{
#if UNITY_WSA && !UNITY_EDITOR
		return WinPlugin.WinApp.GetFileSize(filePath);
#elif !UNITY_WEBPLAYER && !UNITY_WEBGL
		FileInfo fileInfo = new FileInfo(filePath);
		return fileInfo.Length;
#else
		return 0;
#endif
	}

	static void CreateDirectory(string path)
	{
#if UNITY_WSA
		UnityEngine.Windows.Directory.CreateDirectory(path);
#else
		Directory.CreateDirectory(path);
#endif
	}

}
