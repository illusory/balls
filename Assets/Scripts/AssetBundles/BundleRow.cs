﻿using UnityEngine;
using System.Collections;

public class BundleRow
{
	public string bundleName;
	public long bundleSyzeInBytes;
	public int bundleVersion;
	public string bundleHash;
	
	public BundleRow(string newName, long newSize, int newVersion, string newHash)
	{
		bundleName = newName;
		bundleSyzeInBytes = newSize;
		bundleVersion = newVersion;
		bundleHash = newHash;
	}
	
	public BundleRow(string sourceData)
	{
		string[] arguments = sourceData.Split(' ');
		
		if(arguments.Length > 0)
		{
			bundleName = arguments[0];
		}
		if(arguments.Length > 1)
		{
			bundleSyzeInBytes = long.Parse(arguments[1]);
		}
		if(arguments.Length > 2)
		{
			bundleVersion = int.Parse(arguments[2]);
		}
		if(arguments.Length > 3)
		{
			bundleHash = arguments[3];
		}
	}
	
	public void CheckHash(Hash128 newHash)
	{
		if(bundleHash != newHash.GetHashCode().ToString())
		{
			bundleHash = newHash.GetHashCode().ToString();
			bundleVersion++;
		}
	}
	
	public string GetString()
	{
		return bundleName + " " + bundleSyzeInBytes.ToString() + " " + bundleVersion + " " + bundleHash;
	}
}