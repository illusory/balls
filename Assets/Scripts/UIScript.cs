﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIScript : MonoBehaviour
{
	static UIScript instance;

	public Text scoreText;
	public Text difficultText;
	public Text loadingText;
	public Text resultText;
	public GameObject startButton;
	public RectTransform healthBarTransform;
	public SpriteRenderer backgroundImage;

	void Awake()
	{
		instance = this;
	}

	public static void SetScore(int score)
	{
		if(instance != null)
		{
			instance.scoreText.text = "Очки: " + score.ToString ();
		}
	}

	public static void SetHealth(float healthValue)
	{
		if(instance != null)
		{
			instance.healthBarTransform.localScale = new Vector3(healthValue, 1f, 1f);
		}
	}

	public static void SetDifficult(int difficult)
	{
		if (instance != null)
		{
			instance.difficultText.text = "Сложность: " + difficult.ToString ();
		}
	}

	public static void ShowDownloadingProcess()
	{
		if (instance != null)
		{
			instance.scoreText.gameObject.SetActive (false);
			instance.difficultText.gameObject.SetActive (false);
			instance.healthBarTransform.gameObject.SetActive (false);
			instance.backgroundImage.gameObject.SetActive (false);
			instance.resultText.gameObject.SetActive (false);
			instance.startButton.gameObject.SetActive (false);

			instance.loadingText.gameObject.SetActive (true);
		}
	}

	public static void ShowBackgound()
	{
		if (instance != null)
		{
			BundleManager.GetAssetBundleAsync ("background", () =>
			{
				instance.scoreText.gameObject.SetActive (true);
				instance.difficultText.gameObject.SetActive (true);
				instance.healthBarTransform.gameObject.SetActive (true);
				instance.backgroundImage.gameObject.SetActive (true);

				instance.startButton.gameObject.SetActive (false);
				instance.resultText.gameObject.SetActive (false);
				instance.loadingText.gameObject.SetActive (false);

				AssetBundle bundle = BundleManager.GetAssetBundle("background");
				instance.backgroundImage.sprite = bundle.LoadAsset<Sprite>("background");
			});
		}
	}

	public static void ShowResults()
	{
		if (instance != null)
		{
			instance.scoreText.gameObject.SetActive (false);
			instance.difficultText.gameObject.SetActive (false);
			instance.healthBarTransform.gameObject.SetActive (false);
			instance.backgroundImage.gameObject.SetActive (false);

			instance.startButton.gameObject.SetActive (true);
			instance.resultText.gameObject.SetActive (true);

			instance.resultText.text = "Ваш результат: " + GameState.ScoreCount.ToString ();
		}
	}

	public void OnStartButtonClick()
	{
		ShowBackgound ();
		GameState.ResetGame ();
	}
}
