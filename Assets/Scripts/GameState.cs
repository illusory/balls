﻿using UnityEngine;
using System.Collections;

public class GameState
{
	const int maxHealth = 15;
	const int scorePerLevel = 1000;

	static int health;
	public static int Health
	{
		get
		{
			return health;
		}

		set
		{
			health = Mathf.Min(value, maxHealth);
			UIScript.SetHealth ((float)health / (float)maxHealth);
			if(health == 0)
			{
				UnitLoader.instance.DeleteAllObjects ();
				UIScript.ShowResults ();
			}
		}
	}

	static int scoreCount;
	public static int ScoreCount
	{
		get
		{
			return scoreCount;
		}

		set
		{
			scoreCount = value;
			UIScript.SetScore (scoreCount);
			UIScript.SetDifficult (Difficult);
		}
	}

	public static int Difficult
	{
		get
		{
			return 1 + Mathf.FloorToInt((float)(GameState.scoreCount / (float)scorePerLevel));
		}
	}

	public static void ResetGame()
	{
		Health = maxHealth;
		ScoreCount = 0;

		UnitLoader.instance.DeleteAllObjects ();
		UnitLoader.instance.StartSpawn ();
	}
}
