﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureSet
{
	public int referenceCount;
	public Color baseColor;
	public List<Material> generatedMaterials;

	public TextureSet()
	{
		referenceCount = 0;
		generatedMaterials = new List<Material> ();
	}

	public void ReleaseAllMaterials()
	{
		generatedMaterials.Clear ();
	}
}
