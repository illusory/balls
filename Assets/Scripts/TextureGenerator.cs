﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureGenerator
{
	public static Dictionary<int, TextureSet> textureCache = new Dictionary<int, TextureSet> ();

	public static Material GetMaterial(int difficult, float size)
	{
		if (!textureCache.ContainsKey (difficult))
		{
			TextureSet newTextureSet = new TextureSet ();
			float red = Random.value;
			float green = Random.value;
			float blue = Random.value;
			newTextureSet.baseColor = new Color(red, green, blue, 1f);

			newTextureSet.generatedMaterials.Add (CreateMaterial(32, newTextureSet.baseColor, difficult));
			newTextureSet.generatedMaterials.Add (CreateMaterial(64, newTextureSet.baseColor, difficult));
			newTextureSet.generatedMaterials.Add (CreateMaterial(128, newTextureSet.baseColor, difficult));
			newTextureSet.generatedMaterials.Add (CreateMaterial(256, newTextureSet.baseColor, difficult));

			textureCache.Add (difficult, newTextureSet);
		}

		int textureIndex = Mathf.RoundToInt(Mathf.Lerp(0, textureCache[difficult].generatedMaterials.Count - 1, (size - SphereUnit.minSphereSize) / (SphereUnit.maxSphereSize - SphereUnit.minSphereSize)));
		textureCache [difficult].referenceCount++;
		return textureCache[difficult].generatedMaterials[textureIndex];
	}

	public static Color GetBaseColor(int difficult)
	{
		if (textureCache.ContainsKey (difficult))
		{
			return textureCache [difficult].baseColor;
		}
		return Color.white;
	}

	public static void ReleaseMaterial(int difficult)
	{
		if(textureCache.ContainsKey(difficult))
		{
			textureCache [difficult].referenceCount--;
			if(textureCache [difficult].referenceCount <= 0)
			{
				textureCache [difficult].ReleaseAllMaterials ();
				textureCache.Remove (difficult);
				Resources.UnloadUnusedAssets ();
			}
		}
	}

	public static Material CreateMaterial(int sideSize, Color baseColor, int difficult)
	{
		Material newMaterial = new Material (Resources.Load<Material> ("Materials/BaseSphereMaterial"));
		Texture2D newTexture = new Texture2D (sideSize, sideSize, TextureFormat.ARGB32, false);

		Color negative = new Color (1f - baseColor.r, 1f - baseColor.g, 1f - baseColor.b, 1f);
		Color[] textureColors = new Color[sideSize * sideSize];
		for(int i = 0; i < textureColors.Length; i++)
		{
			float x = i % sideSize / (float)sideSize;
			float y = i / sideSize / (float)sideSize;

			float tempColor = Mathf.Sin(x * 40f) + Mathf.Cos(y * 40f);
			tempColor += difficult % 5f;
			tempColor = tempColor % 1f;

			if(sideSize == 32)
			{
				tempColor = (0.5f * (Mathf.Sin ((float)i / 1f) + 1f));
			}
			else if(sideSize == 64)
			{
				tempColor = Mathf.Sin (x * Mathf.PI) + Mathf.Cos (y * 2f * Mathf.PI) * (0.5f * (Mathf.Sin ((float)i / 1f) + 1f)) / 2f ;
			}
			else if(sideSize == 128)
			{
				tempColor = Mathf.Sin (x * 40f) + Mathf.Cos (y * 40f) * (0.5f * (Mathf.Sin ((float)i / 1f) + 1f)) / 2f;
			}

			textureColors [i] = tempColor * baseColor + negative * (1f - tempColor);
		}

		newTexture.SetPixels (textureColors);
		newTexture.Apply ();

		newMaterial.mainTexture = newTexture;

		return newMaterial;
	}
}
