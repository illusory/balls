﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitLoader : MonoBehaviour
{
	public static UnitLoader instance;

	List<GameObject> objectsInScene = new List<GameObject> ();
	GameObject spherePrefab;

	bool isNeedSpawnSpheres = false;
	float maxSpawnTime = 1f;
	float currentTime = 0f;

	void Awake ()
	{
		instance = this;
	}

	public void StartSpawn()
	{
		isNeedSpawnSpheres = true;
	}

	public void DeleteAllObjects()
	{
		for (int i = 0; i < objectsInScene.Count; i++)
		{
			Destroy (objectsInScene[i]);
		}
		objectsInScene.Clear ();
		isNeedSpawnSpheres = false;
	}

	public void SpawnSphere()
	{
		if(spherePrefab == null)
		{
			//Ахтунг синхронная загрузка бандла.
			//Для данного примера, считаю целесообразным использовать синхронную загрузку, но в качесте исключения
			AssetBundle bundle = BundleManager.GetAssetBundle ("sphere_bundle");
			spherePrefab = bundle.LoadAsset<GameObject>("Sphere");
		}

		GameObject newSphere = Instantiate<GameObject>(spherePrefab);

		objectsInScene.Add (newSphere);

		SphereUnit sphereScript = newSphere.GetComponent<SphereUnit> ();
		sphereScript.CustomiseSphere ();
		sphereScript.Place ();
		sphereScript.StartMove ();
	}

	void Update()
	{
		if(isNeedSpawnSpheres)
		{
			if (currentTime > 0)
			{
				currentTime -= Time.deltaTime;
			}
			else
			{
				SpawnSphere ();
				currentTime = maxSpawnTime / (float)GameState.Difficult;
			}
		}
	}
}
