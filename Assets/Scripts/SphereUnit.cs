﻿using UnityEngine;
using System.Collections;

public class SphereUnit : MonoBehaviour
{
	public const float minSphereSize = 2.5f;
	public const float maxSphereSize = 6f;

	public Transform objectTransform;

	float size;
	bool isCanMove = false;
	int createDifficult;

	public void CustomiseSphere()
	{
		createDifficult = GameState.Difficult;
		size = Random.Range (minSphereSize, maxSphereSize);

		float newScale = size / 3f;
		objectTransform.localScale = new Vector3 (newScale, newScale, newScale);

		MeshRenderer sphereRender = gameObject.GetComponent<MeshRenderer> ();
		sphereRender.sharedMaterial = TextureGenerator.GetMaterial (createDifficult , size);
	}

	public void Place()
	{
		float screenHalfWidth = Camera.main.orthographicSize * Camera.main.aspect;
		float xPosition = Random.Range (-screenHalfWidth + size / 4f, screenHalfWidth - size / 4f);
		objectTransform.position = new Vector3 (xPosition, 7f, Random.value * 40f );

		objectTransform.localEulerAngles = new Vector3 (Random.value * 360f, Random.value * 360f, Random.value * 360f);
	}

	public void StartMove()
	{
		isCanMove = true;
	}

	void OnDestroy()
	{
		TextureGenerator.ReleaseMaterial (createDifficult);
	}

	public void OnPlayerClick()
	{
		GameState.ScoreCount += Mathf.RoundToInt(Mathf.Lerp(400f, 100f, (size - minSphereSize) / (maxSphereSize - minSphereSize)));
		GameState.Health++;
		GameObject particle = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/CrashParticle"));
		particle.transform.position = gameObject.transform.position;
		ParticleSystem particleSystem = particle.GetComponent<ParticleSystem> ();
		particleSystem.startColor = TextureGenerator.GetBaseColor(createDifficult);

		GameObject.Destroy (particle, 3f);
		GameObject.Destroy (gameObject);
	}

	void Update()
	{
		if(isCanMove)
		{
			float speed = Mathf.Lerp (3f, 1f, (size - minSphereSize) / (maxSphereSize - 2f));
			objectTransform.position = new Vector3 (objectTransform.position.x, objectTransform.position.y - Time.deltaTime * speed * (1f + 0.1f * (float)GameState.Difficult), objectTransform.position.z);
			if (objectTransform.position.y < -5)
			{
				GameState.Health--;
				GameObject.Destroy (gameObject);
			}
			else
			{
				objectTransform.Rotate (0f, Time.deltaTime * 10f * speed, 0f);
			}
		}
	}
}
