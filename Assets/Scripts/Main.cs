﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{
	void Start()
	{
		if (BundleManager.ReadAssetBundleManifest ())
		{
			if (BundleManager.CheckAssetBundles ())
			{
				OnFinishDownloadingBundles (true);
			}
			else
			{
				UIScript.ShowDownloadingProcess ();
				BundleManager.onFinishDownloadProcess = OnFinishDownloadingBundles;
				BundleManager.Instance.StartCoroutine (BundleManager.DownloadAndCache ());
			}
		}
	}

	public void OnFinishDownloadingBundles(bool isAllAssetsLoadedSuccess)
	{
		UIScript.ShowBackgound ();
		GameState.ResetGame ();
	}
}
