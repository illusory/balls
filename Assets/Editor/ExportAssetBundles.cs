﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

public class ExportAssetBundles : MonoBehaviour
{
	public static string GetBuildPath()
	{
		string buildPath = Application.dataPath + "/AssetBundles/" + BundleManager.GetPlatformName() + "/" + BundleManager.clientAssetBundlesVersion.ToString();
		if(!Directory.Exists(buildPath)){
			Directory.CreateDirectory(buildPath);
		}
		return buildPath;
	}

	public static string GetManifestName()
	{
		return "AssetBundleManifest" + BundleManager.GetPlatformName() + ".txt";
	}

	[MenuItem ("Assets/CreateBundle")]
	static void BuildAllAssetBundles()
	{
		EditorApplication.ExecuteMenuItem ("Assets/Refresh");
		AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles (GetBuildPath(), BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);

		if(!Directory.Exists(Application.dataPath + "/Resources/PluginParams/"))
		{
			Directory.CreateDirectory(Application.dataPath + "/Resources/PluginParams/");
		}

		List<BundleRow> bundlesList = new List<BundleRow>();
		string[] bundleRows = new string[0];
		if(File.Exists(Application.dataPath + "/Resources/PluginParams/" + GetManifestName()))
		{
			bundleRows = File.ReadAllLines(Application.dataPath + "/Resources/PluginParams/" + GetManifestName());
		}
		for(int i = 0; i < bundleRows.Length; i++)
		{
			bundlesList.Add(new BundleRow(bundleRows[i]));
		}

		string[] bundlesInManifest = manifest.GetAllAssetBundles();
		for(int i = 0; i < bundlesInManifest.Length; i++)
		{
			bool isAlreadyInBundleList = false;
			for(int j = 0; j < bundlesList.Count; j++)
			{
				if(bundlesList[j].bundleName == bundlesInManifest[i])
				{
					isAlreadyInBundleList = true;
					bundlesList[j].CheckHash(manifest.GetAssetBundleHash(bundlesInManifest[i]));
					bundlesList[j].bundleSyzeInBytes = BundleManager.GetFileLength(GetBuildPath() + "/" + bundlesInManifest[i]);
					break;
				}
			}
			if(!isAlreadyInBundleList)
			{
				bundlesList.Add(new BundleRow(bundlesInManifest[i], BundleManager.GetFileLength(GetBuildPath() + "/" + bundlesInManifest[i]),
				                              0, manifest.GetAssetBundleHash(bundlesInManifest[i]).GetHashCode().ToString()));
			}
		}

		string[] rowsForSave = new string[bundlesList.Count];
		for(int i = 0; i < rowsForSave.Length; i++)
		{
			rowsForSave[i] = bundlesList[i].GetString();
		}
		File.WriteAllLines(Application.dataPath + "/Resources/PluginParams/" + GetManifestName(), rowsForSave);
	}
}
